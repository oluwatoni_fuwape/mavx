//
//  MavMissionViewModel.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/12/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation

struct MavMissionViewModel{
    
    var mavMission: MavMission!
    
    init(mavMission: MavMission){
        self.mavMission = mavMission
    }
    
    func getMissionId() -> String{
        if let validMissionIds = mavMission.missionIds, validMissionIds.count > 0 {
            return String(validMissionIds[0])
        }
        return ""
    }
    
    func prettifyDate() -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = MavConstants.prettyDateFormat
        return formatter.string(from: stringToDate() )
    }
    
    func getReusedRocketParts() -> String{
        var reusedItems: String = ""
        
        // check for reused core
        if let cores = mavMission.rocket?.firstStage?.cores, cores.count > 0{
            for eachCore in cores{
                if eachCore.reused ?? false{
                    reusedItems += " core |"
                    break
                }
            }
        }
        // check for reused payload
        if let payloads = mavMission.rocket?.secondStage?.payloads, payloads.count > 0{
            for eachPayload in payloads{
                if eachPayload.reused ?? false{
                    reusedItems += " payload |"
                    break
                }
            }
        }
        
        if mavMission.rocket?.fairings?.reused ?? false{
            reusedItems += " fairings |"
        }
        
        if reusedItems.isEmpty{
            return MavConstants.noResuedRocketParts
        }
        return MavConstants.reUsedRocketParts + reusedItems
    }
    
    func stringToDate()-> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = MavConstants.missionDateFormat
        return dateFormatter.date(from: mavMission?.launchDateUtc ?? "") ?? Date()
    }
}

