//
//  AppDelegate.swift
//  MavX
//
//  Created by Toni Fuwape on 8/14/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configViewController()
        return true
    }
    
    func configViewController(){
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let homeViewController = ViewController()
        homeViewController.view.backgroundColor = UIColor.red
        let tabBarCt = UITabBarController()
        
        tabBarCt.setViewControllers([homeViewController], animated: true)
        window!.rootViewController = tabBarCt
        window!.makeKeyAndVisible()
    }
}
