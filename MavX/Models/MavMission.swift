//
//  MavMission.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/11/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation

struct MavMission: MavItemSerializable, MavMultiItemSerializable {
    
    var missionName: String? = ""
    var flightNumber: Int = 0
    var missionIds: [String]? = []
    var launchDateUtc: String? = ""
    var launchDateText: String = ""
    var rocket: Rocket?
    
    //MARK: -
    //MARK: Init
    init?(response: HTTPURLResponse, representation: AnyObject) {
        flightNumber = representation.value(forKeyPath: "flight_number") as? Int ?? 0
        missionName = representation.value(forKeyPath: "mission_name") as? String
        missionIds = representation.value(forKeyPath: "mission_id") as? [String]
        launchDateUtc = representation.value(forKeyPath: "launch_date_utc") as? String
        if let rocketDict = representation.value(forKeyPath: "rocket") as? NSDictionary{
            rocket = Rocket(dictOrNil: rocketDict)
        }
    }
    
    static func collection(response: HTTPURLResponse, representation: AnyObject) -> [MavMission] {
        var mavMissions: [MavMission] = []
        if let mavMissionRepresentations = representation as? [Dictionary<String, AnyObject>] {
            for mavMissionRepresentation in mavMissionRepresentations {
                if let mavMission = MavMission(response: response, representation: mavMissionRepresentation as AnyObject) {
                    mavMissions.append(mavMission)
                }
            }
        }
        return mavMissions
    }
    
}

func ==(lhs: MavMission, rhs: MavMission) -> Bool {
    return lhs.flightNumber == rhs.flightNumber
}

extension MavMission: Hashable {
    var hashValue: Int {
        return flightNumber
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(flightNumber)
    }
}
