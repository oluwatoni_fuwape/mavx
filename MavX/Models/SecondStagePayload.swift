//
//  SecondStagePayload.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/11/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation

struct SecondStagePayload: MavItemSerializable {
    
    var reused: Bool? = false
    
    //MARK: -
    //MARK: Init
    init?(response: HTTPURLResponse, representation: AnyObject) {
        reused = representation.value(forKeyPath: "reused") as? Bool
    }
    
    static func collection(response: HTTPURLResponse, representation: AnyObject) -> [SecondStagePayload] {
        var secondStagePayloads: [SecondStagePayload] = []
        if let secondStagePayloadRepresentations = representation as? [Dictionary<String, AnyObject>] {
            for secondStagePayloadRepresentation in secondStagePayloadRepresentations {
                if let secondStagePayload = SecondStagePayload(response: response, representation: secondStagePayloadRepresentation as AnyObject) {
                    secondStagePayloads.append(secondStagePayload)
                }
            }
        }
        return secondStagePayloads
    }
}
