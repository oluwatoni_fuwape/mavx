//
//  RocketFirstStageCore.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/11/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation

struct FirstStageCore: MavItemSerializable {
    
    var reused: Bool? = false
    
    //MARK: -
    //MARK: Init
    init?(response: HTTPURLResponse, representation: AnyObject) {
        reused = representation.value(forKeyPath: "reused") as? Bool
    }
    
    static func collection(response: HTTPURLResponse, representation: AnyObject) -> [FirstStageCore] {
        var firstStageCores: [FirstStageCore] = []
        if let firstStageCoreRepresentations = representation as? [Dictionary<String, AnyObject>] {
            for firstStageCoreRepresentation in firstStageCoreRepresentations {
                if let firstStageCore = FirstStageCore(response: response, representation: firstStageCoreRepresentation as AnyObject) {
                    firstStageCores.append(firstStageCore)
                }
            }
        }
        return firstStageCores
    }
}
