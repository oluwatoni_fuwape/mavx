//
//  MavRocketFairings.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/11/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation

class Fairings: NSObject, MavItemSerializable {
    
    var reused: Bool? = false
    
    required init?(response: HTTPURLResponse, representation: AnyObject) {
        super.init()
        reused = representation.value(forKeyPath: "reused") as? Bool
    }
    
    init(dictOrNil: NSDictionary?) {
        super.init()
        self.setUpDict(dictOrNil: dictOrNil)
    }
    
    func setUpDict(dictOrNil: NSDictionary? = nil) {
        guard let info = dictOrNil else {return}
        if let reusedValue = info.value(forKeyPath: "reused") as? Bool{
            self.reused = reusedValue
        }
    }
    
}
