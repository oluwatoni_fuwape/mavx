//
//  MavSecondStage.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/11/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation

class SecondStage: NSObject, MavItemSerializable {
    
    var payloads: [SecondStagePayload] = []
    
    required init?(response: HTTPURLResponse, representation: AnyObject) {
        super.init()
        self.setUpDict(dictOrNil: representation,response: response)
    }
    
    init(dictOrNil: NSDictionary?){
        super.init()
        self.setUpDict(dictOrNil: dictOrNil)
    }
    
    func setUpDict(dictOrNil: AnyObject? = nil, response: HTTPURLResponse = HTTPURLResponse()) {
        guard let info = dictOrNil else {return}
        if let payloadsDict = info.value(forKeyPath: "payloads") as? [NSDictionary]{
            payloads = SecondStagePayload.collection(response: HTTPURLResponse(), representation: payloadsDict as AnyObject)
        }
    }
    
}
