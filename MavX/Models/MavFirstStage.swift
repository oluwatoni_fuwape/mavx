//
//  MavFirstStage.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/11/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation

struct MavFirstStage: MavItemSerializable {
    
    var rocketName: String? = ""
    
    //MARK: -
    //MARK: Init
    init?(response: HTTPURLResponse, representation: AnyObject) {
        rocketName = representation.value(forKeyPath: "rocket_name") as? String
    }
    
}
