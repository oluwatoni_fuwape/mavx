//
//  MavFirstStage.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/11/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation

class FirstStage: NSObject, MavItemSerializable {
    
    var cores: [FirstStageCore] = []
    
    required init?(response: HTTPURLResponse, representation: AnyObject) {
        super.init()
        self.setUpDict(dictOrNil: representation,response: response)
    }
    
    init(dictOrNil: NSDictionary?){
        super.init()
        self.setUpDict(dictOrNil: dictOrNil)
    }
    
    func setUpDict(dictOrNil: AnyObject? = nil, response: HTTPURLResponse = HTTPURLResponse()) {
        guard let info = dictOrNil else {return}
        if let coresDict = info.value(forKeyPath: "cores") as? [NSDictionary]{
            cores = FirstStageCore.collection(response: HTTPURLResponse(), representation: coresDict as AnyObject)
        }
    }
    
}
