//
//  Rocket.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/11/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation

class Rocket: NSObject,MavItemSerializable {
    
    var rocketName: String? = ""
    var firstStage: FirstStage?
    var secondStage: SecondStage?
    var fairings: Fairings?
    
    required init?(response: HTTPURLResponse, representation: AnyObject) {
        super.init()
        self.setUpDict(dictOrNil: representation as? NSDictionary)
    }
    
    init(dictOrNil: NSDictionary?){
        super.init()
        self.setUpDict(dictOrNil: dictOrNil)
    }
    
    func setUpDict(dictOrNil: NSDictionary? = nil) {
        guard let info = dictOrNil else {return}
        if let rocketNameVal = info.value(forKeyPath: "rocket_name") as? String{
            rocketName = rocketNameVal
        }
        if let firstStageDict = info.value(forKeyPath: "first_stage") as? NSDictionary{
            firstStage = FirstStage(dictOrNil: firstStageDict)
        }
        if let secondStageDict = info.value(forKeyPath: "second_stage") as? NSDictionary{
            secondStage = SecondStage(dictOrNil: secondStageDict)
        }
        if let fairingsDict = info.value(forKeyPath: "fairings") as? NSDictionary{
            fairings = Fairings(dictOrNil: fairingsDict)
        }
    }

}
