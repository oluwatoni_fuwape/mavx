//
//  MavVideoServiceReachable.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/11/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation
import Alamofire

protocol MavXServiceReachable {
    
    //displayable?
    func fetchNextMission(completion: @escaping (MavMission?) -> ())
    func fetchUpcomingMissions(completion: @escaping ([MavMission]?) -> ())
}

extension Request: MavAPIRequest {}
extension MavNetworkService: MavXServiceReachable {
    
    func fetchNextMission(completion: @escaping ((MavMission)?) -> ()) {
        if let url = URL(string: baseURL + MavConstants.nextMissionName) {
            let mutableURLRequest = NSMutableURLRequest(url: url)
            mutableURLRequest.httpMethod = HTTPMethod.get.rawValue
            Alamofire.request(mutableURLRequest.url!, headers: headers)
                .responseObject{ (response: DataResponse<MavMission>) in
                    guard let mission = response.result.value else {
                        completion(nil)
                        return
                    }
                    completion(mission)
            }
        }
    }
    
    func fetchUpcomingMissions(completion: @escaping ([MavMission]?) -> ()) {
        if let url = URL(string: baseURL + MavConstants.upcomingMissionsName) {
            let mutableURLRequest = NSMutableURLRequest(url: url)
            mutableURLRequest.httpMethod = HTTPMethod.get.rawValue
            Alamofire.request(mutableURLRequest.url!, headers: headers)
                .responseCollection { (response: DataResponse<[MavMission]>) in
                    guard let item = response.result.value else {
                        completion(nil)
                        return
                    }
                    let typedArray = item.map {$0 as MavMission}
                    completion(typedArray)
            }
        }
    }
    

}
