//
//  MavAPIRequest.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/11/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation
import Alamofire

public protocol MavAPIRequest {
    var task: URLSessionTask? {get}
    func suspend()
    func resume()
    func cancel()
}

