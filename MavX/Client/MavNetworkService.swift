//
//  MavNetworkService.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/11/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation
import Alamofire

public protocol MavItemSerializable {
    init?(response: HTTPURLResponse, representation: AnyObject)
}

public protocol MavMultiItemSerializable {
    static func collection(response: HTTPURLResponse, representation: AnyObject) -> [Self]
}

extension DataRequest {
    static func objectResponseSerializer<U: MavItemSerializable>() -> DataResponseSerializer<U> {
        return DataResponseSerializer { req, res, data, error in
            return Request.serlializedResponseObject(request: req, response: res, data: data, error: error)
        }
    }
    
    static func collectionResponseSerializer<U: MavMultiItemSerializable>() -> DataResponseSerializer<[U]> {
        return DataResponseSerializer { req, res, data, error in
            return Request.serlializedResponseCollection(request: req, response: res, data: data, error: error)
        }
    }
    
    @discardableResult
    func responseObject<U: MavItemSerializable>(completionHandler: @escaping (DataResponse<U>) -> Void) -> Self {
        return response(responseSerializer: DataRequest.objectResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseCollection<U: MavMultiItemSerializable>(completionHandler: @escaping (DataResponse<[U]>) -> Void) -> Self {
        return response(responseSerializer: DataRequest.collectionResponseSerializer(), completionHandler: completionHandler)
    }
}

extension Request {
    static func serlializedResponseObject<T: MavItemSerializable>(request: URLRequest?, response: HTTPURLResponse?, data: Data?, error: Error?) -> Result<T> {
        if let error = error {return .failure(error)}
        let jsonResponse = Request.serializeResponseJSON(options: .allowFragments, response: response, data: data, error: error)
        switch jsonResponse {
        case .failure(let error):
            return .failure(error)
        case .success(let value):
            guard let response = response, let value = value as? NSDictionary else {
                return .failure(AFError.responseSerializationFailed(reason: AFError.ResponseSerializationFailureReason.inputDataNil))
            }
            if let errorValue = value["error"] as? NSDictionary {
                let parsedError = MavNetworkServiceError(withDictionary: errorValue)
                return .failure(parsedError)
            } else if let dataValue = value["data"], let obj = T(response: response, representation: dataValue as AnyObject) {
                return .success(obj)
            } else if let paginatorValue = value["paginator"], let paginator = T(response: response, representation: paginatorValue as AnyObject)  {
                return .success(paginator)
            } else if let userValue = value["message"], let user = T(response: response, representation: userValue as AnyObject)  {
                return .success(user)
            } else if let someValue = T(response: response, representation: value as AnyObject) {
                return .success(someValue)
            } else {
                let serializationError = NSError(domain: "com.mav.modelSerializationError", code: 0, userInfo: nil)
                return .failure(AFError.responseSerializationFailed(reason: AFError.ResponseSerializationFailureReason.jsonSerializationFailed(error: serializationError)))
            }
        }
    }
    
    static func serlializedResponseCollection<T: MavMultiItemSerializable>(request: URLRequest?, response: HTTPURLResponse?, data: Data?, error: Error?) -> Result<[T]> {
        if let error = error { return .failure(error) }
        let jsonResponse = Request.serializeResponseJSON(options: .allowFragments, response: response, data: data, error: error)
        switch jsonResponse {
        case .failure(let error):
            return .failure(error)
        case .success(let value):
            guard let response = response, let value = value as? NSArray else {
                let noResponseError = NSError(domain: "com.mav.error", code: 0, userInfo: nil)
                return .failure(noResponseError)
            }
            return .success(T.collection(response: response, representation: value as AnyObject))
        }
    }
}


struct MavNetworkService {
    let baseURL: String
    var headers: HTTPHeaders {
        return [:]
    }
    init(){
        guard let infoDictionary = Bundle.main.infoDictionary, let dynamicBaseURL = infoDictionary["APIBaseURLEndpoint"] as? String else {
            baseURL = MavConstants.baseEndPointUrl
            return
        }
        baseURL = dynamicBaseURL
    }
}


