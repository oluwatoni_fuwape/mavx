//
//  MavNetworkServiceError.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/11/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation

struct MavNetworkServiceError: Error {
    static let Domain = "com.mav.networkservice"
    enum MavNetworkServiceErrorCode: Int {
        case badRequest = 400
        case unauthorized = 401
        case forbidden = 403
        case notFound = 404
        case methodNotAllowed = 405
        case notAcceptable = 406
        case gone = 410
        case teapot = 418
        case tooManyRequests = 429
        case internalServerError = 500
        case serviceUnavailable = 503
    }
    
    fileprivate let code: MavNetworkServiceErrorCode?
    let failureReason: String
    
    init(withErrorCode errorCode: MavNetworkServiceErrorCode? = .badRequest) {
        code = errorCode
        failureReason = MavNetworkServiceError.failureReason(from: errorCode)
    }
    
    init(withDictionary dictionary: NSDictionary) {
        guard let apiErrorCode = dictionary["status_code"] as? Int else {
            code = .badRequest
            failureReason = MavNetworkServiceError.failureReason(from: .badRequest)
            return
        }
        let derivedNetworkError = MavNetworkServiceErrorCode(rawValue: apiErrorCode)
        code = derivedNetworkError != nil ? derivedNetworkError! : .badRequest
        var derivedFailureReason = MavNetworkServiceError.failureReason(from: code)
        if let message = dictionary["message"] as? String {
            derivedFailureReason = message
        } else if let messageDictionary = dictionary["message"] as? NSDictionary {
            if let baseErrorArray = messageDictionary["base"] as? NSArray, let message = baseErrorArray[0] as? String {
                derivedFailureReason = message
            } else if let emailErrorArray = messageDictionary["email"] as? NSArray, let message = emailErrorArray[0] as? String {
                derivedFailureReason = message
            } else {
                derivedFailureReason = "An unknown error occured. Please try again later."
            }
        }
        failureReason = derivedFailureReason
    }
    
    private static func failureReason(from code: MavNetworkServiceErrorCode?) -> String {
        var failureReason = "An error occured. Please try again later."
        guard let errorCode = code else {return failureReason}
        switch errorCode {
        case .badRequest:
            failureReason = "Bad Request – Your request is incorrect"
            break
        case .unauthorized:
            failureReason = "Unauthorized – Your API key is wrong"
            break
        case .forbidden:
            failureReason = "Forbidden – The resource requested is hidden for administrators only"
            break
        case .notFound:
            failureReason = "Not Found – The specified resource could not be found"
            break
        case .methodNotAllowed:
            failureReason = "Method Not Allowed – You tried to access a resource with an invalid method"
            break
        case .notAcceptable:
            failureReason = "Not Acceptable – You requested a format that isn’t json"
            break
        case .gone:
            failureReason = "Gone – The resource requested has been removed from our servers"
            break
        case .teapot:
            failureReason = "I’m a teapot"
            break
        case .tooManyRequests:
            failureReason = "Too Many Requests – You’re requesting too many resources! Slown down!"
            break
        case .internalServerError:
            failureReason = "Internal Server Error – We had a problem with our server. Try again later."
            break
        case .serviceUnavailable:
            failureReason = "Service Unavailable – We’re temporarially offline for maintanance. Please try again later."
            break
        }
        return failureReason
    }
}
