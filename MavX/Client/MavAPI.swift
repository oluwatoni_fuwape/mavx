//
//  MavAPI.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/11/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

protocol MavMissionNetworkServiceProvidable {
    var maxMissionNetworkService: MavXServiceReachable { get }
}

protocol MavXNetworkServiceProvidable: MavMissionNetworkServiceProvidable {}

struct MavAPI: MavXNetworkServiceProvidable {
    fileprivate var networkService: MavNetworkService {
        return MavNetworkService()
    }
    
    var maxMissionNetworkService: MavXServiceReachable {
        return networkService
    }

}

