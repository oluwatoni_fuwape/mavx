//
//  Constants.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/12/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation
import UIKit

struct MavConstants {
    
    static let countDownTitle: String = "Countdown to Next Mission"
    static let countDownDy: String = "Days:"
    static let countDownHr: String = "Hours:"
    static let countDownMin: String = "Minutes:"
    static let countDownSec: String = "Seconds:"
    
    static let mavTableViewCellID: String = "MavTableViewCell"
    
    static let timerTitleFontSize: CGFloat = 17.0
    static let timerNumberOfLines: Int = 0
    
    static let missionCellHeight: CGFloat = 120.0
    
    static let mavTitle: String = "MavX Missions"
    
    static let baseEndPointUrl: String = "https://api.spacexdata.com/v3/launches/"
    static let upcomingMissionsName: String = "upcoming"
    static let nextMissionName: String = "next"
    
    static let noResuedRocketParts: String = "No Reused Rocket Pieces"
    static let reUsedRocketParts: String = "Reused Rocket Pieces: "
    
    static let missionDateFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    static let prettyDateFormat: String = "EEEE, MMMM, d, yyyy,  h:mm a"
    
    static let serverErrorTitle: String = "Server Error"
    static let serverErrorMessagePrefix: String = "An error occured while fetching "
    static let serverErrorMessagePostfix: String = "\nPlease try again"
    

}
