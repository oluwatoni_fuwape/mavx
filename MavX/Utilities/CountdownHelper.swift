//
//  CountdownHelper.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/12/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation
import UIKit

class CountdownHelper {
    
    fileprivate var timerLabel: UILabel!
    fileprivate var timerTitleLabel: UILabel!
    fileprivate var countdownTimer: Timer!
    fileprivate var nextMissionVM: MavMissionViewModel?
    fileprivate var mavDelegate: MavMissionDataSourceDelegate!
    
    init(timerLabel: UILabel, timerTitle: UILabel, mavDelegate: MavMissionDataSourceDelegate) {
        self.timerLabel = timerLabel
        self.timerTitleLabel = timerTitle
        self.mavDelegate = mavDelegate
    }
    
    func update(withNextMission nextMission: MavMission) {
        self.nextMissionVM = MavMissionViewModel(mavMission: nextMission)
        timerTitleLabel.text = "\n\n\(MavConstants.countDownTitle):\n \(nextMissionVM?.mavMission.missionName ?? "")"
        startTimer()
    }
    
    // Timer Handler
    @objc func updateTime() {
        timerLabel.text = timeFormatted()
        if let mSeconds = nextMissionVM?.stringToDate().timeIntervalSince1970, mSeconds<=0 {
            endTimer()
            mavDelegate.loadNextMission()
        }
    }
    
    func endTimer() {
        countdownTimer.invalidate()
    }
    
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    func timeFormatted() -> String {
        if let nextDate = nextMissionVM?.stringToDate(){
            return countDownText(nextDate: nextDate)
        }
        return ""
    }
    
    func countDownText(nextDate: Date, dateNow: Date = Date()) -> String{
        let interval = nextDate.timeIntervalSince(dateNow)
       
        let days =  (interval / (60*60*24)).rounded(.down)
        let daysRemainder = interval.truncatingRemainder(dividingBy: 60*60*24).rounded(.down)
        
        let hours = (daysRemainder / (60 * 60)).rounded(.down)
        let hoursRemainder = daysRemainder.truncatingRemainder(dividingBy: 60 * 60).rounded(.down)
        
        let minutes  = (hoursRemainder / 60).rounded(.down)
        let minutesRemainder = hoursRemainder.truncatingRemainder(dividingBy: 60).rounded(.down)
        
        let seconds = minutesRemainder.truncatingRemainder(dividingBy: 60).rounded(.down)
        
        return "\(MavConstants.countDownDy) \(Int(days)),  \(MavConstants.countDownHr) \(Int(hours))\n\(MavConstants.countDownMin) \(Int(minutes)), \(MavConstants.countDownSec) \(Int(seconds))"
    }
    
}
