//
//  ViewController.swift
//  MavX
//
//  Created by Toni Fuwape on 8/14/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import UIKit
import SnapKit
import PopupDialog

class ViewController: UIViewController {
    
    var tableView: UITableView!
    fileprivate var timerTitleLabel: UILabel!
    fileprivate var timerLabel: UILabel!
    fileprivate let apiClient = MavAPI()
    fileprivate var mavMissionDataSource: MavMissionDataSource!
    fileprivate var countDownTimerHelper: CountdownHelper?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTimerLabel()
        configureTableView()
        configureStackView()
        self.title = MavConstants.mavTitle
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        countDownTimerHelper?.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        countDownTimerHelper?.endTimer()
    }
    
    func configureStackView(){
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.backgroundColor = UIColor.white
        view.addSubview(stackView)
        
        stackView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        stackView.addArrangedSubview(self.timerTitleLabel)
        stackView.addArrangedSubview(self.timerLabel)
        stackView.addArrangedSubview(self.tableView)
    }
    
    func configureTableView(){
        self.tableView = UITableView(frame: .zero, style: .plain)
        self.tableView.register(MavTableViewCell.self, forCellReuseIdentifier: MavConstants.mavTableViewCellID)
        mavMissionDataSource = MavMissionDataSource(withClient: apiClient, withDelegate: self)
        self.tableView.dataSource = mavMissionDataSource
        self.tableView.delegate = self
    }
    
    func configureTimerLabel(){
        timerTitleLabel = UILabel(frame: .zero)
        timerTitleLabel.font = UIFont.boldSystemFont(ofSize: MavConstants.timerTitleFontSize)
        timerTitleLabel.text = MavConstants.countDownTitle
        timerLabel = UILabel(frame: .zero)
        
        for eachLabel in [timerTitleLabel,timerLabel]{
            eachLabel?.backgroundColor = UIColor.orange
            eachLabel?.textColor = UIColor.white
            eachLabel?.textAlignment = NSTextAlignment.center
            eachLabel?.numberOfLines = MavConstants.timerNumberOfLines
        }
        self.countDownTimerHelper = CountdownHelper(timerLabel: timerLabel,timerTitle: timerTitleLabel, mavDelegate: self)
    }
    
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MavConstants.missionCellHeight
    }
}

extension ViewController: MavMissionDataSourceDelegate{
    func hasUpcomingMissions() {
        tableView.reloadData()
    }
    
    func hasNextMission() {
        if let nextMission = mavMissionDataSource.nextMission{
            countDownTimerHelper?.update(withNextMission: nextMission)
        }
    }
    
    func loadNextMission() {
        mavMissionDataSource.loadNextMission()
    }
    
    func showErrorDialog(isUpcoming: Bool){
        
        let errorText = isUpcoming ? "Upcoming Missions" : "Next Mission"
        let message = "\(MavConstants.serverErrorMessagePrefix) \(errorText) \(MavConstants.serverErrorMessagePostfix)"
        let popup = PopupDialog(title: MavConstants.serverErrorTitle, message: message, image: nil)
        
        let buttonCancel = CancelButton(title: "CANCEL") {}
        let buttonRetry = DefaultButton(title: "RETRY") {
            if isUpcoming{
                self.mavMissionDataSource.loadUpcomingItems()
            }else{
                self.mavMissionDataSource.loadNextMission()
            }
        }
        popup.addButtons([buttonRetry, buttonCancel])
        self.present(popup, animated: true, completion: nil)
    }
    
}
