//
//  MavMissionDataSourceDelegate.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/12/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation

protocol MavMissionDataSourceDelegate {
    func hasUpcomingMissions()
    func hasNextMission()
    func loadNextMission()
    func showErrorDialog(isUpcoming: Bool)
}
