//
//  MavMissionDataSource.swift
//  MavFarmXSpaceX
//
//  Created by Toni Fuwape on 8/11/19.
//  Copyright © 2019 Oluwatoni Fuwape. All rights reserved.
//

import Foundation
import UIKit

class MavMissionDataSource: NSObject, UITableViewDataSource{
    
    fileprivate var upcommingMissions: [MavMission]?
    var nextMission: MavMission?
    
    fileprivate var fetchRequest: MavAPIRequest? {
        willSet {
            guard let request = fetchRequest else {return}
            request.suspend()
        } didSet {
            guard let request = fetchRequest else {return}
            request.resume()
        }
    }
    
    fileprivate let client: MavXNetworkServiceProvidable
    fileprivate let mavMissionDataSourceDelegate: MavMissionDataSourceDelegate
    
    init(withClient client: MavXNetworkServiceProvidable, withDelegate deletage: MavMissionDataSourceDelegate){
        self.client = client
        self.mavMissionDataSourceDelegate = deletage
        super.init()
        self.loadUpcomingItems()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.upcommingMissions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MavConstants.mavTableViewCellID, for: indexPath) as? MavTableViewCell
        if let item = self.upcommingMissions?[indexPath.item]{
            let viewModel = MavMissionViewModel(mavMission: item)
            cell?.textLabel?.text = "Mission Name: " + (item.missionName ?? "")
            
            var detailText = "Mission ID: " + viewModel.getMissionId() + "\n"
            detailText += "Mission Time: " + viewModel.prettifyDate() + "\n"
            detailText += "Rocket Name: " + (item.rocket?.rocketName ?? "") + "\n"
            detailText += viewModel.getReusedRocketParts()
            cell?.detailTextLabel?.text = detailText
        }

        return cell!
    }
    
    func loadUpcomingItems(){
        client.maxMissionNetworkService.fetchUpcomingMissions {[weak self] (upItems) in
            self?.upcommingMissions = upItems
            guard let _ = upItems else {
                self?.mavMissionDataSourceDelegate.showErrorDialog(isUpcoming: true)
                return
            }
            self?.mavMissionDataSourceDelegate.hasUpcomingMissions()
            self?.loadNextMission()
        }
    }
    
    func loadNextMission(){
        client.maxMissionNetworkService.fetchNextMission {[weak self] (nextMisson) in
            self?.nextMission = nextMisson
            guard let _ = nextMisson else {
                self?.mavMissionDataSourceDelegate.showErrorDialog(isUpcoming: false)
                return
            }
            self?.mavMissionDataSourceDelegate.hasNextMission()
        }
    }
    
}
